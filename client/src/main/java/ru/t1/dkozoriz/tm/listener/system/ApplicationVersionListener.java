package ru.t1.dkozoriz.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.system.ServerVersionRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    public ApplicationVersionListener() {
        super("version", "show version info.", "-v");
    }

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        System.out.println("version: " + systemEndpoint.getVersion(new ServerVersionRequest()).getVersion());
    }

}