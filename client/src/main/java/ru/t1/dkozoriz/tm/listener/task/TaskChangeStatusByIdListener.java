package ru.t1.dkozoriz.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

import java.util.Arrays;


@Component
public final class TaskChangeStatusByIdListener extends AbstractTaskListener {

    public TaskChangeStatusByIdListener() {
        super("task-change-status-by-id", "change task status by id.");
    }

    @Override
    @EventListener(condition = "@taskChangeStatusByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        taskEndpoint.taskChangeStatusById(new TaskChangeStatusByIdRequest(getToken(), id, status));
    }

}