package ru.t1.dkozoriz;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dkozoriz.marker.IntegrationCategory;
import ru.t1.dkozoriz.tm.client.ProjectRestEndpointClient;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.model.Project;


@Category(IntegrationCategory.class)
public class ProjectTest {

    private static final ProjectRestEndpointClient client = ProjectRestEndpointClient.getInstance();
    private final Project project1 = new Project("Project 1", "Description 1");
    private final Project project2 = new Project("Project 2", "Description 2");

    @Before
    public void before() {
        client.post(project1);
    }

    @After
    public void after() {
        client.delete(project1.getId());
    }


    @Test
    public void getByIdTest() {
        Assert.assertNotNull(client.get(project1.getId()));
    }

    @Test
    public void addTest() {
        final Project project = new Project("Test Project", "Test");
        client.post(project);
        Assert.assertNotNull(client.get(project.getId()));
        client.delete(project.getId());
    }

    @Test
    public void deleteByIdTest() {
        client.post(project2);
        long expected = client.count() - 1;
        client.delete(project2.getId());
        Assert.assertEquals(expected, (long)client.count());
    }

    @Test
    public void editTest() {
        Assert.assertNotNull(client.get(project1.getId()));
        project1.setName("New name");
        project1.setStatus(Status.IN_PROGRESS);
        project1.setDescription("New Description");
        client.put(project1);
        final Project updatedProject = client.get(project1.getId());
        Assert.assertEquals(project1.getId(), updatedProject.getId());
        Assert.assertEquals(project1.getStatus(), updatedProject.getStatus());
        Assert.assertEquals(project1.getDescription(), updatedProject.getDescription());
    }

    @Test
    public void getAllTest() {
        Assert.assertEquals(client.getAll().size(), (long)client.count());
    }

    @Test
    public void countTest() {
        long count = client.count() + 1;
        final Project project = new Project("Test Project", "Test");
        client.post(project);
        Assert.assertEquals(count, (long)client.count());
    }

}