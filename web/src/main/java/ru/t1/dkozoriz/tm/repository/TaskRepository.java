package ru.t1.dkozoriz.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    Task findByUserIdAndId(final String userId, final String id);

    void deleteByUserIdAndId(final String userId, final String id);

    void deleteByUserId(final String userId);

    List<Task> findAllByUserId(final String userId);

    long countByUserId(final String userId);

}