package ru.t1.dkozoriz.tm;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.dkozoriz.tm.configuration.ApplicationConfiguration;
import ru.t1.dkozoriz.tm.configuration.WebApplicationConfiguration;
import ru.t1.dkozoriz.tm.configuration.WebConfig;

import javax.servlet.ServletContext;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ApplicationConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebApplicationConfiguration.class, WebConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected void registerContextLoaderListener(final ServletContext servletContext) {
        super.registerContextLoaderListener(servletContext);
    }

}