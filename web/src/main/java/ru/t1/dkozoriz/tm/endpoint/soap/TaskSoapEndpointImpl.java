package ru.t1.dkozoriz.tm.endpoint.soap;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.dkozoriz.tm.dto.soap.*;
import ru.t1.dkozoriz.tm.model.Task;
import ru.t1.dkozoriz.tm.service.TaskService;
import ru.t1.dkozoriz.tm.util.UserUtil;


@Endpoint
@RequiredArgsConstructor
public class TaskSoapEndpointImpl {

    private final TaskService taskService;

    public static final String LOCATION_URI = "/ws";

    public static final String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    public static final String NAMESPACE = "http://tm.dkozoriz.t1.ru/dto/soap";

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskClearRequest", namespace = NAMESPACE)
    public TaskClearResponse clear(@RequestPayload final TaskClearRequest request) {
        taskService.deleteAll(UserUtil.getUserId());
        return new TaskClearResponse();
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskCountRequest", namespace = NAMESPACE)
    public TaskCountResponse count(@RequestPayload final TaskCountRequest request) {
        final TaskCountResponse response = new TaskCountResponse();
        response.setCountTask(taskService.count(UserUtil.getUserId()));
        return response;
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskCreateRequest", namespace = NAMESPACE)
    private TaskCreateResponse create(@RequestPayload final TaskCreateRequest request) {
        final TaskCreateResponse response = new TaskCreateResponse();
        final Task task = new Task(request.getName());
        taskService.save(UserUtil.getUserId(), task);
        return response;
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@RequestPayload final TaskDeleteByIdRequest request) {
        taskService.deleteById(UserUtil.getUserId(), request.getId());
        return new TaskDeleteByIdResponse();
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(@RequestPayload final TaskFindAllRequest request) {
        final TaskFindAllResponse response = new TaskFindAllResponse();
        response.setTasks(taskService.findAll(UserUtil.getUserId()));
        return response;
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(@RequestPayload final TaskFindByIdRequest request) {
        final TaskFindByIdResponse response = new TaskFindByIdResponse();
        final Task task = taskService.findById(UserUtil.getUserId(), request.getId());
        response.setTask(task);
        return response;
    }

    @ResponsePayload
    @PreAuthorize("isAuthenticated()")
    @PayloadRoot(localPart = "taskSaveRequest", namespace = NAMESPACE)
    private TaskSaveResponse save(@RequestPayload final TaskSaveRequest request) {
        taskService.update(UserUtil.getUserId(), request.getTask());
        return new TaskSaveResponse();
    }

}